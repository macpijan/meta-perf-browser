DESCRIPTION = "core-image-weston with WPE"
SUMMARY = "A very basic Wayland image with a terminal and WPE"

IMAGE_FEATURES += "splash package-management hwcodecs ssh-server-openssh"

LICENSE = "MIT"

inherit core-image distro_features_check

REQUIRED_DISTRO_FEATURES = "wayland"

CORE_IMAGE_BASE_INSTALL += "weston weston-init"

EXTRA_IMAGE_FEATURES .= " debug-tweaks"
# EXTRA_IMAGE_FEATURES .= " debug-tweaks dbg-pkgs tools-debug tools-profile"
# By default, the Yocto build system strips symbols from the binaries it
# packages, which makes it difficult to use some of the tools.
# 
# You can prevent that by setting the INHIBIT_PACKAGE_STRIP variable to "1" in
# your local.conf when you build the image:
# INHIBIT_PACKAGE_STRIP = "1"

IMAGE_INSTALL_append = " libtasn1 htop nano strace bridge-utils ntp"
# IMAGE_INSTALL_append = " glmark2"
# IMAGE_INSTALL_append = " mesa-demos"
IMAGE_INSTALL_append = " wpewebkit"
IMAGE_INSTALL_append = " gstreamer1.0-plugins-base gstreamer1.0-plugins-base-meta"
IMAGE_INSTALL_append = " gstreamer1.0-plugins-good gstreamer1.0-plugins-good-meta"
IMAGE_INSTALL_append = " gstreamer1.0-plugins-bad  gstreamer1.0-plugins-bad-meta"
IMAGE_INSTALL_append = " gstreamer1.0-libav"
IMAGE_INSTALL_append = " cog"

IMAGE_FEATURES += "ssh-server-openssh"

# IMAGE_FSTYPES += " wic"

IMAGE_LINGUAS = "en-us es-es"

DISTRO_FEATURES_append = " opengl wayland"

# IMAGE_OVERHEAD_FACTOR = "1.5"
# IMAGE_ROOTFS_EXTRA_SPACE = "2097152"
